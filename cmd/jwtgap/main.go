package main

import (
	"fmt"
	"github.com/kelseyhightower/envconfig"
	. "gitlab.com/los-romka/jwtgap/internal"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

func main() {
	log.SetFlags(0)
	log.SetOutput(new(logWriter))

	var s specification

	if err := envconfig.Process("jwtgap", &s); err != nil {
		log.Fatal(err.Error())
	}

	publicKey, err := ioutil.ReadFile(s.PublicKey)

	if err != nil {
		log.Fatal(err.Error())
	}

	middleware, err := NewJWTAuthMiddleware(publicKey, s.JwtCookieName)

	if err != nil {
		log.Fatal(err.Error())
	}

	serveMux := http.DefaultServeMux
	serveMux.HandleFunc("/health", healthHandler)
	serveMux.HandleFunc("/", middleware.Handler)

	log.Printf("Go: %s\n", makeHttpServerUrl(s.ServerAddr))

	log.Fatal(http.ListenAndServe(s.ServerAddr, serveMux))
}

type specification struct {
	ServerAddr    string `envconfig:"LISTEN_ADDR" required:"true" default:":8080"`
	PublicKey     string `envconfig:"PUBLIC_KEY" required:"true"`
	JwtCookieName string `envconfig:"JWT_COOKIE_NAME"`
}

func healthHandler(writer http.ResponseWriter, req *http.Request) {
	_, err := io.WriteString(writer, "jwtgap is healthy!")

	if err != nil {
		log.Fatal(err.Error())
	}
}

func makeHttpServerUrl(serverAddr string) string {
	if serverAddr[0] == ':' {
		return "http://127.0.0.1" + serverAddr
	}

	return "http://" + serverAddr
}

type logWriter struct {
}

func (writer logWriter) Write(bytes []byte) (int, error) {
	return fmt.Print(time.Now().UTC().Format("2006-01-02T15:04:05.999Z") + " [DEBUG] " + string(bytes))
}
