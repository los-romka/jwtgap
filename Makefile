BINPATH := $(CURDIR)/bin/jwtgap

CGO_ENABLED ?= 0

APP_ENV ?= prod
LISTEN_ADDR ?= ":8080"

.PHONY: build
build: build/$(APP_ENV)

.PHONY: build/prod
build/prod:
	@echo "--> Building jwtgap production binary..."
	@CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -o $(BINPATH) ./cmd/jwtgap

.PHONY: build/dev
build/dev:
	@echo "--> Building jwtgap development binary..."
	@go build -race -o $(BINPATH) ./cmd/jwtgap

.PHONY: run
run: build
	@$(BINPATH)

.PHONY: test
test:
	@echo "--> Testing jwtgap..."
	@go test -count=1 -coverprofile=coverage.out ./internal/... ; cat coverage.out | awk 'BEGIN {cov=0; stat=0;} $$3!="" { cov+=($$3==1?$$2:0); stat+=$$2; } END {printf("Total coverage: %.2f%% of statements\n", (cov/stat)*100);}'

.PHONY: lint
lint:
	@go fmt ./...
	@go vet ./...

.PHONY: release
release:
	@docker-compose -f deployments/docker-compose/docker-compose.prod.yml build --force-rm
	@docker-compose -f deployments/docker-compose/docker-compose.prod.yml push

.PHONY: format
format:
	@gofmt -s -w .

# disallow any parallelism (-j) for Make. This is necessary since some
# commands during the build process create temporary files that collide
# under parallel conditions.
.NOTPARALLEL:
