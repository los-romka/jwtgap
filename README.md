# jwtgap

JWT Golang Auth Proxy

### Переменные окружения jwtgap

- `LISTEN_ADDR` - адрес сервера jwtgap. Например, `127.0.0.1:8080` или `:8080`.
- `PUBLIC_KEY` - путь до публичного jwt-ключа (RS256).
- `JWT_COOKIE_NAME` - название куки, где хранится jwt. Может быть пустым.

### HTTP-заголовки для jwtgap

- `jwtgap-Required-Roles` - список обязательных ролей в JWT:payload:roles, которые требуются для успешного запроса. Например, `["ROLE_ADMIN", "ROLE_USER"]`.
- `jwtgap-Fallback-Url` - адрес куда будет перенаправлен пользователь, если есть проблемы с токеном. Например, `http://127.0.0.1:8000/login/`.

### Детали работы

- Если токена нет, то `HttpStatusCode: 307` -> `jwtgap-Fallback-Url`
- Если токена протух, то `HttpStatusCode: 307` -> `jwtgap-Fallback-Url`
- Если токен невалиден, то `HttpStatusCode: 307` -> `jwtgap-Fallback-Url`
- Если токен валиден и нет требуемых ролей, то `HttpStatusCode: 403`
- Если токен валиден и есть требуемые роли, то `X-Accel-Redirect` на запрашиваемый ресурс

### Переменные окружения make

- `APP_ENV` - режим работы `{dev|prod}`
- Переменные окружения jwtgap

### Команды make

- `make build` - собрать бинарник в корень проекта. Конфигурация сборки зависит от `APP_ENV`.
- `make run` - собрать бинарник в корень проекта и запустить. Конфигурация сборки зависит от `APP_ENV`.
- `make format` - отформатировать исходники.
- `make lint` - проверить форматирование исходников.
- `make test` - запуск тестов с CodeCoverage.

### Отладка

Простейший способ отладки

```bash
APP_ENV=dev JWT_COOKIE_NAME=BEARER PUBLIC_KEY=$(pwd)/internal/testdata/public.pem LISTEN_ADDR=":8888" make run
```
