package internal

import (
	"crypto/rsa"
	"encoding/json"
	"fmt"
	"github.com/golang-jwt/jwt/v4"
	"github.com/golang-jwt/jwt/v4/request"
	"log"
	"net/http"
)

type JWTAuthMiddleware struct {
	verifyKey      *rsa.PublicKey
	tokenExtractor request.Extractor
}

type ApplicationClaims struct {
	Roles    []string `json:"roles"`
	Username string   `json:"username"`
	jwt.StandardClaims
}

const (
	headerFallbackUrl   = "jwtgap-Fallback-Url"
	headerRequiredRoles = "jwtgap-Required-Roles"
)

type jwtgapResult string

const (
	resultOk           jwtgapResult = "Ok"
	resultInvalidToken              = "InvalidToken"
	resultAccessDenied              = "AccessDenied"
	resultBadGateway                = "BadGateway"
)

func NewJWTAuthMiddleware(verifyKey []byte, jwtCookieName string) (JWTAuthMiddleware, error) {
	publicKey, err := jwt.ParseRSAPublicKeyFromPEM(verifyKey)

	if err != nil {
		return JWTAuthMiddleware{}, err
	}

	return JWTAuthMiddleware{
		publicKey,
		&request.MultiExtractor{
			request.AuthorizationHeaderExtractor,
			CookieExtractor(jwtCookieName),
		},
	}, nil
}

type CookieExtractor string

func (e CookieExtractor) ExtractToken(req *http.Request) (string, error) {
	if e != "" {
		if cookie, err := req.Cookie(string(e)); err == nil {
			return cookie.Value, nil
		}
	}

	return "", request.ErrNoTokenInRequest
}

func (m *JWTAuthMiddleware) extractJwtGapHeaders(req *http.Request) (
	fallbackUrl string,
	roles []string,
	err error,
) {
	if err = json.Unmarshal(([]byte)(req.Header.Get(headerRequiredRoles)), &roles); err != nil {
		return
	}

	fallbackUrl = req.Header.Get(headerFallbackUrl)

	if fallbackUrl == "" {
		err = fmt.Errorf("empty fallbackUrl")
	}

	return
}

func (m *JWTAuthMiddleware) Handler(writer http.ResponseWriter, req *http.Request) {
	var fallbackUrl string
	var requiredRoles []string
	var token string
	var err error
	var logMessage string
	var res = resultOk

	fallbackUrl, requiredRoles, err = m.extractJwtGapHeaders(req)

	logMessage = fmt.Sprintf("%s %s ### User-Agent: %v ### Authorization: %v ### Cookies: %v", req.Method, req.RequestURI, req.UserAgent(), req.Header.Get("Authorization"), req.Cookies())
	if err != nil {
		res = resultBadGateway
		log.Printf("%s ### %s", logMessage, err.Error())
	} else {
		token, err = m.tokenExtractor.ExtractToken(req)

		if err != nil {
			res = resultInvalidToken
			log.Printf("%s ### %s", logMessage, err.Error())
		} else {
			res = m.validateToken(token, requiredRoles)
			log.Printf("%s ### %s", logMessage, res)
		}
	}

	switch res {
	case resultOk:
		writer.Header().Add("X-Accel-Redirect", req.RequestURI)
	case resultAccessDenied:
		writer.WriteHeader(http.StatusForbidden)
	case resultInvalidToken:
		http.Redirect(writer, req, fallbackUrl, http.StatusTemporaryRedirect)
	case resultBadGateway:
		writer.WriteHeader(http.StatusBadGateway)
	}
}

func (m *JWTAuthMiddleware) validateToken(tokenString string, requiredRoles []string) jwtgapResult {
	token, err := jwt.ParseWithClaims(tokenString, &ApplicationClaims{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		return m.verifyKey, nil
	})

	if err != nil {
		return resultInvalidToken
	}

	if claims, ok := token.Claims.(*ApplicationClaims); ok && token.Valid {
		for _, v1 := range requiredRoles {
			flag := false

			for _, v2 := range claims.Roles {
				if v1 == v2 {
					flag = true
					break
				}
			}

			if !flag {
				return resultAccessDenied
			}
		}
	} else {
		return resultInvalidToken
	}

	return resultOk
}
