package internal

import (
	"github.com/golang-jwt/jwt/v4"
	"io/ioutil"
	"log"
	"os"
	"testing"
	"time"
)

var (
	middleware *JWTAuthMiddleware

	privateKey []byte
	publicKey  []byte
)

func TestJWTAuthMiddleware_validateToken(t *testing.T) {
	tests := []struct {
		name        string
		tokenString string
		want        jwtgapResult
	}{
		{
			name:        "admin",
			tokenString: "fake token",
			want:        resultInvalidToken,
		},
		{
			name:        "user",
			tokenString: createToken("user", []string{"ROLE_USER"}, time.Now().Add(time.Minute*1).Unix()),
			want:        resultAccessDenied,
		},
		{
			name:        "admin",
			tokenString: createToken("admin", []string{"ROLE_ADMIN"}, time.Now().Add(time.Minute*-1).Unix()),
			want:        resultInvalidToken,
		},
		{
			name:        "admin",
			tokenString: createToken("admin", []string{"ROLE_ADMIN"}, time.Now().Add(time.Minute*1).Unix()),
			want:        resultOk,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name+":  "+tt.tokenString, func(t *testing.T) {
			if got := middleware.validateToken(tt.tokenString, []string{"ROLE_ADMIN"}); got != tt.want {
				t.Errorf("validateToken() = %v, want %v", got, tt.want)
			}
		})
	}
}

func createToken(username string, roles []string, expiresAt int64) string {
	claims := ApplicationClaims{
		roles,
		username,
		jwt.StandardClaims{ExpiresAt: expiresAt},
	}

	token := jwt.NewWithClaims(jwt.GetSigningMethod("RS256"), claims)
	signKey, _ := jwt.ParseRSAPrivateKeyFromPEM(privateKey)

	tokenString, err := token.SignedString(signKey)

	if err != nil {
		log.Fatal("error")
	}

	return tokenString
}

func TestMain(m *testing.M) {
	privateKey, _ = ioutil.ReadFile("testdata/private.pem")
	publicKey, _ = ioutil.ReadFile("testdata/public.pem")

	middlewareObj, _ := NewJWTAuthMiddleware(
		publicKey,
		"BEARER",
	)

	middleware = &middlewareObj

	code := m.Run()
	os.Exit(code)
}
